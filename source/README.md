**1.** Clone the repo located at [here](https://gitlab.com/xabush/ghost). This repo contains the **GHOST** rules and their respective functions.

**2.** Clone the *guile-json* library from [here](https://github.com/Habush/guile-json).

**2a.** Build the guile-json library using the following commands

    autoreconf

    ./configure --prefix=/usr

     make

     sudo make install



**3.** Start **relex** server

**4.** You will also need to clone the [bio-data](https://gitlab.com/opencog-bio/bio-data) repo that contains the datasets and GO atoms

**5.** Open the **.guile** file in your home directory and add the following lines. Make sure you provide the appropraite path:

        (use-modules (ice-9 readline))
        (activate-readline)
        (add-to-load-path "/usr/local/share/opencog/scm")
        (add-to-load-path "/usr/local/share/opencog/scm/opencog")
        (add-to-load-path ".")
        (use-modules (opencog))
        (use-modules (opencog query))
        (use-modules (opencog exec))
        (use-modules (opencog) (opencog bioscience) (opencog openpsi))
        (use-modules (opencog)
        (opencog nlp relex2logic)
        (opencog openpsi)
        (opencog eva-behavior))

        (use-modules (opencog ghost) (opencog python))
        (use-modules (srfi srfi-1))
        (use-modules (ice-9 regex))
        (use-modules (json))

        (primitive-load "/path/to/ghost/repo/pm_functions.scm")
        (primitive-load "/path/to/ghost/repo/utils.scm")
        (primitive-load "/path/to/ghost/repo/ghost_test.scm")

        (primitive-load "/path/to/bio-data/scm-representations/current/GO.scm")
        (primitive-load "/path/to/bio-data/scm-representations/current/GO_annotation.scm")

        (ghost-parse-file "/path/to/ghost/repo/chatrules.top")

**6.** Clone the REST API located [here](https://gitlab.com/icog-labs/mozi_backend_flask/tree/master) and follow the instructions there to set it up.

**7.** Clone the Mozi frontend repo from [here](https://gitlab.com/icog-labs/mozi)  and install the dependencies using `npm install`

**8.** Edit the `app.config.ts` file so that the **opencog_url**  property of the configs object is pointing to the locally running the OpenCog REST API server. It should look like this:

    export const configs = {
        "url" : "http://localhost:5000/api/v1.1",
        "opencog_url": "http://localhost:5000/api/v1.1"
    };

**9.** Congratulations! If you have made it to this step without any error, you have now everything setup properly. All you have to do now is start the Mozi frontend using ng serve and browse to http://localhost:4200