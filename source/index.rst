.. Mozi documentation master file, created by
   sphinx-quickstart on Mon Mar 19 13:19:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mozi's documentation!
================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme_link


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
